#!/usr/bin/env groovy

node {

    stage('Checkout') {
        checkout scm
        def packageJSON = readJSON file: 'package.json'
        if (packageJSON) {
            echo "Build version ${packageJSON.version}"
        }
    }

    stage("Install NODE") {
        withEnv(["PATH+NODE=${tool 'NodeJSLocal'}/bin"]) {
            sh "npm install"
        }    
    }
    
    stage("Build") {
        withEnv(["PATH+NODE=${tool 'NodeJSLocal'}/bin"]) {
            sh "npm run build-ci"
            archiveArtifacts artifacts: 'dist/**'
        }    
    }

    stage("Test Coverage") {
        withEnv(["PATH+NODE=${tool 'NodeJSLocal'}/bin"]) {
            if(env.COVERAGE_TEST) {
                sh "npm run test-ci"
                archiveArtifacts artifacts: 'coverage/**/*'
            } else {
                sh "npm run test-ci-without-coverage"
            }
        }
    }

    stage('Sonar scanner') {
        withEnv(["PATH+NODE=${tool 'NodeJSLocal'}/bin"]) {
            sh "/var/jenkins_home/.sonar/bin/sonar-scanner -Dsonar.organization=marcbariller -Dsonar.projectKey=MarcBariller_examen15052020 -Dsonar.sources=. -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=${SONAR_LOGIN}"
        }
    }
    
}
